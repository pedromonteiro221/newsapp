package com.pedromonteiro.newsapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.transition.Fade;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pedromonteiro.newsapp.domain.DataModelDTO;
import com.pedromonteiro.newsapp.fragments.ArticleFragment;
import com.pedromonteiro.newsapp.fragments.ArticleListFragment;
import com.pedromonteiro.newsapp.utils.AnimationUtils;
import com.pedromonteiro.newsapp.viewmodels.ArticleViewModel;
import com.pedromonteiro.newsapp.viewmodels.AuthenticationViewModel;

import java.util.concurrent.Executor;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.fragment_container_view)
    FragmentContainerView fragmentContainerView;

    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private AuthenticationViewModel authenticationViewModel;

    @BindView(R.id.auth_container)
    LinearLayout authContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        authenticationViewModel = new ViewModelProvider(this).get(AuthenticationViewModel.class);
        if (!authenticationViewModel.isAuthenticated()) {
            // Show authentication dialog and show in activity a button in case the user cancels
            showBiometricPrompt();
        } else {
            // Show Articles List Fragment
            showArticleListFragment();
        }

        authContainer.setOnClickListener(view -> showBiometricPrompt());

        ArticleViewModel articleViewModel = new ViewModelProvider(this).get(ArticleViewModel.class);
        articleViewModel.getSelectedArticle().observe(this, new Observer<DataModelDTO.ArticleDTO>() {
            @Override
            public void onChanged(DataModelDTO.ArticleDTO articleDTO) {
                if (articleDTO != null) {
                    showArticleFragment();
                }
            }
        });
    }

    private void showBiometricPrompt() {
        AnimationUtils.fadeIn(authContainer, this);

        Executor executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(MainActivity.this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                Toast.makeText(getApplicationContext(), "Authentication succeeded!", Toast.LENGTH_SHORT).show();
                authenticationViewModel.setAuthenticated(true);
                showArticleListFragment();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Biometric authentication")
                .setSubtitle("Please authenticate yourself using biometric crendetials.")
                .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG | BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                .build();

        biometricPrompt.authenticate(promptInfo);
    }

    private void showArticleListFragment() {
        AnimationUtils.fadeOut(authContainer, this);

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("ArticleListFragment");

        if (fragment == null) {
            ArticleListFragment articleListFragment = ArticleListFragment.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container_view, articleListFragment, "ArticleListFragment");
            fragmentTransaction.commit();
        }
    }

    private void showArticleFragment() {
        // Check if fragment was already added
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("ArticleFragment");
        if (fragment == null) {
            // Show Article Fragment
            ArticleFragment articleFragment = ArticleFragment.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(
                    R.anim.slide_in, R.anim.fade_out,
                    R.anim.fade_in, R.anim.slide_out
            );
            fragmentTransaction.replace(R.id.fragment_container_view, articleFragment, "ArticleFragment");
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0)
            finish();
        else
            super.onBackPressed();
    }
}