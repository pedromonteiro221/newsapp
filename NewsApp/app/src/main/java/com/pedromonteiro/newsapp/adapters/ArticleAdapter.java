package com.pedromonteiro.newsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pedromonteiro.newsapp.R;
import com.pedromonteiro.newsapp.domain.DataModelDTO;
import com.pedromonteiro.newsapp.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ArticleHolder> {
    private List<DataModelDTO.ArticleDTO> articleDTOList = new ArrayList<>();
    private ArticleRecyclerViewCallback callback;

    public interface ArticleRecyclerViewCallback {
        void onClick(DataModelDTO.ArticleDTO articleDTO, ArticleHolder holder);
    }

    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_item, parent, false);
        return new ArticleHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleHolder holder, int position) {
        DataModelDTO.ArticleDTO currentArticle = articleDTOList.get(position);

        if (!Utils.stringIsNullOrEmpty(currentArticle.getUrlToImage()))
            Picasso.get().load(currentArticle.getUrlToImage()).into(holder.articleImage);

        holder.articleTitle.setText(currentArticle.getTitle());
        holder.articlePublishedAt.setText(Utils.getTimeAgo(currentArticle.getPublishedAt()));

        if (callback != null)
            holder.itemView.setOnClickListener(view -> callback.onClick(currentArticle, holder));
    }

    @Override
    public int getItemCount() {
        return articleDTOList.size();
    }

    public void setArticleDTOList(List<DataModelDTO.ArticleDTO> articleDTOList) {
        this.articleDTOList = articleDTOList;
        notifyDataSetChanged();
    }

    public void setCallback(ArticleRecyclerViewCallback callback) {
        this.callback = callback;
    }

    public static class ArticleHolder extends RecyclerView.ViewHolder {
        private ImageView articleImage;
        private TextView articleTitle;
        private TextView articlePublishedAt;

        public ArticleHolder(@NonNull View itemView) {
            super(itemView);
            this.articleImage = itemView.findViewById(R.id.article_image);
            this.articleTitle = itemView.findViewById(R.id.article_title);
            this.articlePublishedAt = itemView.findViewById(R.id.article_published_at);
        }

        public ImageView getArticleImage() {
            return articleImage;
        }
    }
}
