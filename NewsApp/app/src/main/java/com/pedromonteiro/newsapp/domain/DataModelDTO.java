package com.pedromonteiro.newsapp.domain;

import java.util.Date;

public class DataModelDTO {

    public static class ArticleDTO {
        private SourceDTO source;
        private String author;
        private String title;
        private String description;
        private String url;
        private String urlToImage;
        private Date publishedAt;
        private String content;

        public SourceDTO getSource() {
            return source;
        }

        public String getAuthor() {
            return author;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getUrl() {
            return url;
        }

        public String getUrlToImage() {
            return urlToImage;
        }

        public Date getPublishedAt() {
            return publishedAt;
        }

        public String getContent() {
            return content;
        }
    }

    public static class SourceDTO {
        private String id;
        private String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
