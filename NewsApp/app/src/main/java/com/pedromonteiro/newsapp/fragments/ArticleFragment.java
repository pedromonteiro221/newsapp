package com.pedromonteiro.newsapp.fragments;

import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pedromonteiro.newsapp.R;
import com.pedromonteiro.newsapp.domain.DataModelDTO;
import com.pedromonteiro.newsapp.utils.Utils;
import com.pedromonteiro.newsapp.viewmodels.ArticleViewModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticleFragment extends Fragment {

    private ArticleViewModel articleViewModel;

    @BindView(R.id.back_button)
    ImageButton backButton;
    @BindView(R.id.article_image)
    ImageView articleImage;
    @BindView(R.id.article_title)
    TextView articleTitle;
    @BindView(R.id.article_published_at)
    TextView articlePublishedAt;
    @BindView(R.id.article_description)
    TextView articleDescription;
    @BindView(R.id.article_content)
    TextView articleContent;

    public static ArticleFragment newInstance() {
        return new ArticleFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.article_fragment, container, false);

        ButterKnife.bind(this, fragmentView);

        if (articleViewModel.getSelectedArticle() != null && articleViewModel.getSelectedArticle().getValue() != null)
            setupData(articleViewModel.getSelectedArticle().getValue());

        backButton.setOnClickListener(view -> requireActivity().getSupportFragmentManager().popBackStack());

        return fragmentView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.articleViewModel = new ViewModelProvider(requireActivity()).get(ArticleViewModel.class);
    }

    private void setupData(DataModelDTO.ArticleDTO articleDTO) {

        if (!Utils.stringIsNullOrEmpty(articleDTO.getUrlToImage()))
            Picasso.get().load(articleDTO.getUrlToImage()).into(articleImage);

        articleTitle.setText(articleDTO.getTitle());

        articleDescription.setText(articleDTO.getDescription());

        articlePublishedAt.setText(Utils.getFormattedDate(articleDTO.getPublishedAt()));

        if (!Utils.stringIsNullOrEmpty(articleDTO.getContent()))
            articleContent.setText(articleDTO.getContent());
        else
            articleContent.setVisibility(View.GONE);

    }
}