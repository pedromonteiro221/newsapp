package com.pedromonteiro.newsapp.fragments;

import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.pedromonteiro.newsapp.R;
import com.pedromonteiro.newsapp.adapters.ArticleAdapter;
import com.pedromonteiro.newsapp.domain.DataModelDTO;
import com.pedromonteiro.newsapp.utils.AnimationUtils;
import com.pedromonteiro.newsapp.viewmodels.ArticleListViewModel;
import com.pedromonteiro.newsapp.viewmodels.ArticleViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticleListFragment extends Fragment {

    private ArticleListViewModel articleListViewModel;
    private ArticleViewModel articleViewModel;
    private ArticleAdapter articleAdapter;
    private Context context;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.container_no_data)
    LinearLayout containerNoData;
    @BindView(R.id.message)
    TextView messageTextView;
    @BindView(R.id.refresh_button)
    MaterialButton refreshButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    public static ArticleListFragment newInstance() {
        return new ArticleListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.article_list_fragment, container, false);
        ButterKnife.bind(this, fragmentView);

        articleViewModel.removeSelectedArticle();

        showProgress();

        setupRecyclerViewAdapter();

        setupListData();

        return fragmentView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.articleListViewModel = new ViewModelProvider(this).get(ArticleListViewModel.class);
        this.articleViewModel = new ViewModelProvider(requireActivity()).get(ArticleViewModel.class);
    }

    private void setupRecyclerViewAdapter() {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        articleAdapter = new ArticleAdapter();
        recyclerView.setAdapter(articleAdapter);

        articleAdapter.setCallback(new ArticleAdapter.ArticleRecyclerViewCallback() {
            @Override
            public void onClick(DataModelDTO.ArticleDTO articleDTO, ArticleAdapter.ArticleHolder holder) {
                articleViewModel.selectArticle(articleDTO);
            }
        });
    }

    private void setupListData() {
        articleListViewModel.getArticleList(new ArticleListViewModel.ArticleListViewModelCallback() {
            @Override
            public void onSuccess(List<DataModelDTO.ArticleDTO> articleDTOList) {
                // Show Recycler View layout and set new list
                if (articleDTOList.size() > 0) {
                    showList();
                    articleAdapter.setArticleDTOList(articleDTOList);
                } else {
                    showMessage("There are currently no articles. Please try again later");
                }

            }

            @Override
            public void onError(String message) {
                // Show Error in screen
                showMessage(message);
            }
        });
    }

    private void showMessage(String message) {
        messageTextView.setText(message);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgress();

                articleListViewModel.refresh();
            }
        });

        AnimationUtils.fadeOut(recyclerView, context);

        AnimationUtils.fadeIn(containerNoData, context);

        AnimationUtils.fadeOut(progressBar, context);
    }

    private void showProgress() {
        AnimationUtils.fadeOut(recyclerView, context);

        AnimationUtils.fadeOut(containerNoData, context);

        AnimationUtils.fadeIn(progressBar, context);
    }

    private void showList() {
        AnimationUtils.fadeIn(recyclerView, context);

        AnimationUtils.fadeOut(containerNoData, context);

        AnimationUtils.fadeOut(progressBar, context);
    }
}