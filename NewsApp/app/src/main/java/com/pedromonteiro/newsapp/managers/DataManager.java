package com.pedromonteiro.newsapp.managers;

import androidx.annotation.NonNull;

import com.pedromonteiro.newsapp.domain.DataModelDTO;
import com.pedromonteiro.newsapp.services.NewsApi;
import com.pedromonteiro.newsapp.services.RequestListeners;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class DataManager {

    private static volatile DataManager instance;

    public static DataManager getInstance() {
        if (instance == null) {
            synchronized (DataManager.class) {
                if (instance == null) {
                    instance = new DataManager();
                }
            }
        }

        return instance;
    }

    public DataManager() {
        if (instance != null)
            throw new RuntimeException("DataManager(): Use getInstance() method to get the singleton of this class");
    }

    public Single<List<DataModelDTO.ArticleDTO>> getTopHeadlines() {
        return new Single<List<DataModelDTO.ArticleDTO>>() {
            @Override
            protected void subscribeActual(@NonNull SingleObserver<? super List<DataModelDTO.ArticleDTO>> observer) {
                NewsApi.getInstance().getTopHeadlines(new RequestListeners.RESTRequestListener<List<DataModelDTO.ArticleDTO>>() {
                    @Override
                    public void onSuccess(List<DataModelDTO.ArticleDTO> response) {
                        Collections.sort(response, new Comparator<DataModelDTO.ArticleDTO>() {
                            @Override
                            public int compare(DataModelDTO.ArticleDTO a1, DataModelDTO.ArticleDTO a2) {
                                return a2.getPublishedAt().compareTo(a1.getPublishedAt());
                            }
                        });
                        observer.onSuccess(response);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        observer.onError(new Throwable(errorMessage));
                    }
                });
            }
        };
    }
}
