package com.pedromonteiro.newsapp.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pedromonteiro.newsapp.BuildConfig;
import com.pedromonteiro.newsapp.domain.DataModelDTO;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewsApi {

    private static volatile NewsApi instance;

    private NewsApiServices newsApi;

    public static NewsApi getInstance() {
        if (instance == null) {
            synchronized (NewsApi.class) {
                if (instance == null)
                    instance = new NewsApi();
            }
        }

        return instance;
    }

    public NewsApi() {
        if (instance != null)
            throw new RuntimeException("NewsApi(): Use getInstance() method to get the singleton of this class");

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                .create();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(BuildConfig.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(10L, TimeUnit.SECONDS)
                .readTimeout(10L, TimeUnit.SECONDS)
                .writeTimeout(10L, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

        retrofitBuilder.client(httpClientBuilder.build());

        Retrofit retrofit = retrofitBuilder.build();
        newsApi = retrofit.create(NewsApiServices.class);
    }

    public void getTopHeadlines(final RequestListeners.RESTRequestListener<List<DataModelDTO.ArticleDTO>> listener) {
        Disposable disposable = newsApi.getTopHeadlines(BuildConfig.COUNTRY_CODE, BuildConfig.API_KEY)
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<TopHeadlinesResponseDTO>() {
                    @Override
                    public void accept(TopHeadlinesResponseDTO topHeadlinesResponseDTO) throws Exception {
                        if (topHeadlinesResponseDTO.getStatus().equals("ok"))
                            listener.onSuccess(topHeadlinesResponseDTO.getArticles());
                        else
                            listener.onError("An internal error has occurred, please try again later.");
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        if (throwable instanceof UnknownHostException || throwable instanceof SocketTimeoutException || throwable instanceof ConnectException)
                            listener.onError("There are problems when trying to connect to the server, please try again later.");
                        else
                            listener.onError("An internal error has occurred, please try again later.");
                    }
                });
    }
}
