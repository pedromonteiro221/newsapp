package com.pedromonteiro.newsapp.services;


import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApiServices {

    @GET("v2/top-headlines")
    Observable<TopHeadlinesResponseDTO> getTopHeadlines(@Query("country") String countryCode,
                                                        @Query("apiKey") String apiKey);
}
