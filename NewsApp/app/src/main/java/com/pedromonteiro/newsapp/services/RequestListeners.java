package com.pedromonteiro.newsapp.services;

public class RequestListeners {

    public interface BaseRequestListener {
        void onError(String errorMessage);
    }

    public interface RESTRequestListener<T> extends BaseRequestListener {
        void onSuccess(T response);
    }
}
