package com.pedromonteiro.newsapp.services;

import com.pedromonteiro.newsapp.domain.DataModelDTO;

import java.util.List;

public class TopHeadlinesResponseDTO {
    private String status;
    private Integer totalResults;
    private List<DataModelDTO.ArticleDTO> articles;

    public String getStatus() {
        return status;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public List<DataModelDTO.ArticleDTO> getArticles() {
        return articles;
    }
}
