package com.pedromonteiro.newsapp.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.view.View;

public class AnimationUtils {
    public static void fadeOut(final View mView, final Context context) {
        if (mView.getVisibility() == View.GONE)
            return;

        mView.setVisibility(View.VISIBLE);
        mView.setAlpha(1.0f);

        mView.animate()
                .setDuration(context.getResources().getInteger(android.R.integer.config_shortAnimTime))
                .alpha(0.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mView.setVisibility(View.GONE);
                    }
                }).start();
    }

    public static void fadeIn(final View mView, final Context context) {
        mView.setVisibility(View.VISIBLE);
        mView.setAlpha(0.0f);

        mView.animate()
                .setDuration(context.getResources().getInteger(android.R.integer.config_shortAnimTime))
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mView.setVisibility(View.VISIBLE);
                    }
                }).start();
    }
}
