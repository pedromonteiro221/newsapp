package com.pedromonteiro.newsapp.utils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static String getTimeAgo(Date date) {
        String output = "1 min ago";

        if (date != null) {
            long different = System.currentTimeMillis() - date.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % hoursInMilli;

            if (elapsedDays > 0)
                output = elapsedDays + (elapsedDays > 1 ? " days ago" : " day ago");
            else if (elapsedHours > 0)
                output = elapsedHours + (elapsedHours > 1 ? " hours ago" : " hour ago");
            else if (elapsedMinutes > 0)
                output = elapsedMinutes + (elapsedMinutes > 1 ? " mins ago" : " min ago");
        }

        return output;
    }

    public static boolean stringIsNullOrEmpty(String string) {
        if (string == null)
            return true;

        return string.isEmpty();
    }

    public static String getFormattedDate(Date date) {
        if (date == null)
            date = new Date();

        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd MMMM, yyyy");

        try {
            return df.format(date);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return "";
        }
    }
}
