package com.pedromonteiro.newsapp.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pedromonteiro.newsapp.domain.DataModelDTO;
import com.pedromonteiro.newsapp.managers.DataManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class ArticleListViewModel extends ViewModel {

    private MutableLiveData<List<DataModelDTO.ArticleDTO>> _articleList = new MutableLiveData<>();
    private LiveData<List<DataModelDTO.ArticleDTO>> articleList = _articleList;
    private ArticleListViewModelCallback callback;

    public interface ArticleListViewModelCallback {
        void onSuccess(List<DataModelDTO.ArticleDTO> articleDTOList);

        void onError(String message);
    }

    public void getArticleList(ArticleListViewModelCallback callback) {
        this.callback = callback;
        if (articleList.getValue() == null || (articleList.getValue() != null && articleList.getValue().size() == 0)) {
            requestTopHeadlines();
        } else {
            callback.onSuccess(articleList.getValue());
        }
    }

    public void refresh() {
        requestTopHeadlines();
    }

    private void requestTopHeadlines() {
        Disposable disposable = DataManager.getInstance().getTopHeadlines()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<DataModelDTO.ArticleDTO>>() {
                    @Override
                    public void accept(List<DataModelDTO.ArticleDTO> articleDTOS) throws Exception {
                        _articleList.postValue(articleDTOS);

                        if (callback != null)
                            callback.onSuccess(articleDTOS);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        _articleList.postValue(null);

                        if (callback != null)
                            callback.onError(throwable.getMessage());
                    }
                });
    }
}
