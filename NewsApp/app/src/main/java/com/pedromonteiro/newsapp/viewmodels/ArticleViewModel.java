package com.pedromonteiro.newsapp.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pedromonteiro.newsapp.adapters.ArticleAdapter;
import com.pedromonteiro.newsapp.domain.DataModelDTO;

public class ArticleViewModel extends ViewModel {
    private MutableLiveData<DataModelDTO.ArticleDTO> _selectedArticle = new MutableLiveData<DataModelDTO.ArticleDTO>();
    private LiveData<DataModelDTO.ArticleDTO> selectedArticle = _selectedArticle;

    public void selectArticle(DataModelDTO.ArticleDTO article) {
        _selectedArticle.postValue(article);
    }

    public LiveData<DataModelDTO.ArticleDTO> getSelectedArticle() {
        return selectedArticle;
    }

    public void removeSelectedArticle() {
        _selectedArticle.postValue(null);
    }
}