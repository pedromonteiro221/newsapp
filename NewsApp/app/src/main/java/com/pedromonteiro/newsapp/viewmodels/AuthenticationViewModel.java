package com.pedromonteiro.newsapp.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.lifecycle.AndroidViewModel;

public class AuthenticationViewModel extends AndroidViewModel {
    private boolean isAuthenticated = false;

    public AuthenticationViewModel(@NonNull Application application) {
        super(application);
    }

    private boolean canAuthenticate() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
            BiometricManager biometricManager = BiometricManager.from(getApplication());

            return biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG | BiometricManager.Authenticators.DEVICE_CREDENTIAL) == BiometricManager.BIOMETRIC_SUCCESS;
        }

        return false;
    }

    public void setAuthenticated(boolean authenticated) {
        isAuthenticated = authenticated;
    }

    public boolean isAuthenticated() {
        if (canAuthenticate())
            return isAuthenticated;

        return true;
    }
}
