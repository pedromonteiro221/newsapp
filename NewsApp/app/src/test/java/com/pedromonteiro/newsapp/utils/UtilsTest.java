package com.pedromonteiro.newsapp.utils;



import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;


public class UtilsTest {

    @Test
    public void getTimeAgo_nullDate() {
        String string = Utils.getTimeAgo(null);

        assertThat(string).isEqualTo("1 min ago");
    }

    @Test
    public void getTimeAgo_sameDate() {
        String string = Utils.getTimeAgo(new Date());

        assertThat(string).isEqualTo("1 min ago");
    }

    @Test
    public void getTimeAgo_pluralDates() {
        // Minutes
        Calendar calendarMinutes = Calendar.getInstance();
        calendarMinutes.add(Calendar.MINUTE, -5);

        String pluralMinutes = Utils.getTimeAgo(calendarMinutes.getTime());
        assertThat(pluralMinutes).isEqualTo("5 mins ago");

        // Hours
        Calendar calendarHours = Calendar.getInstance();
        calendarHours.add(Calendar.HOUR, -23);

        String pluralHours = Utils.getTimeAgo(calendarHours.getTime());
        assertThat(pluralHours).isEqualTo("23 hours ago");

        // Days
        Calendar calendarDays = Calendar.getInstance();
        calendarDays.add(Calendar.DAY_OF_MONTH, -10);

        String pluralDays = Utils.getTimeAgo(calendarDays.getTime());
        assertThat(pluralDays).isEqualTo("10 days ago");
    }

    @Test
    public void getTimeAgo_singularDates() {
        // Minute
        Calendar calendarMinutes = Calendar.getInstance();
        calendarMinutes.add(Calendar.MINUTE, -1);

        String pluralMinutes = Utils.getTimeAgo(calendarMinutes.getTime());
        assertThat(pluralMinutes).isEqualTo("1 min ago");

        // Hour
        Calendar calendarHours = Calendar.getInstance();
        calendarHours.add(Calendar.HOUR, -1);

        String pluralHours = Utils.getTimeAgo(calendarHours.getTime());
        assertThat(pluralHours).isEqualTo("1 hour ago");

        // Day
        Calendar calendarDays = Calendar.getInstance();
        calendarDays.add(Calendar.DAY_OF_MONTH, -1);

        String pluralDays = Utils.getTimeAgo(calendarDays.getTime());
        assertThat(pluralDays).isEqualTo("1 day ago");
    }

    @Test
    public void stringIsNullOrEmpty_nullString() {
        boolean b = Utils.stringIsNullOrEmpty(null);

        assertThat(b).isEqualTo(true);
    }

    @Test
    public void stringIsNullOrEmpty_emptyString() {
        boolean b = Utils.stringIsNullOrEmpty("");

        assertThat(b).isEqualTo(true);
    }

    @Test
    public void stringIsNullOrEmpty_contentString() {
        boolean b = Utils.stringIsNullOrEmpty("content");

        assertThat(b).isEqualTo(false);
    }
}